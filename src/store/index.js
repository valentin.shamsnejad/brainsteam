import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    botMessage: null
  },
  mutations: {
    setBotMessage (context, message) {
      this.state.botMessage = message
    }
  },
  actions: {
  },
  modules: {
  }
})
