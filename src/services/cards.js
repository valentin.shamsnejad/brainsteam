import YAML from 'yaml'
import axios from 'axios'

export default {
  async getDeck (fileName) {
    const response = await axios.get(`decks/${fileName}.yml`)
    return YAML.parse(response.data)
  }
}
