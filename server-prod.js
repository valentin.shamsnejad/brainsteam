const express = require('express')
const serveStatic = require('serve-static')
const path = require('path')
var history = require('connect-history-api-fallback')
const port = process.env.PORT || 8081

let app = express()

const getRandomInt = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}
const pages = ['aimed', 'chillin', 'lifeward']

app.get('/', (req, res) => {
  const rand = getRandomInt(0, pages.length - 1)
  const name = pages[rand]
  res.redirect(301, `/ab/${name}`)
})

app.use(history())
app.use('/', serveStatic(path.join(__dirname, 'dist')))
app.use(function (request, response) {
  if (!request.secure) {
    response.redirect('https://' + request.headers.host + request.url)
  }
})

// Backend endpoints
// var routes = require('./api/routes')
// var bodyParser = require('body-parser')
// app.use(bodyParser.urlencoded({ extended: true }))
// app.use(bodyParser.json())
// routes(app)

app.listen(port, () => {
  console.log('App is running on port ' + port)
})
