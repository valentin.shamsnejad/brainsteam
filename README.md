# brainsteam

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run the production server
```
npm run build
npm run start
```

### Auth0
[auth0 for spa vuejs](https://auth0.com/docs/quickstart/spa/vuejs/01-login)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
